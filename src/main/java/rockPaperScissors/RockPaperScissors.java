package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while(true) {
            //Create output for new round.
            System.out.println("Let's play round " + roundCounter);
            //Get user's choice.
            String userChoice = getUserChoice();
            //Get computer's choice.
            String computerChoice = getComputerChoice();

            //Get the result from a round, then print it.
            String outcome; 
            if(isWinner(userChoice, computerChoice)) {
                //Whenever user wins.
                outcome = "Human wins!";
                humanScore++;
            }else if (isWinner(userChoice, computerChoice)) {
                //Whenever computer wins.
                outcome = "Computer wins!";
                computerScore++;
            }else {
                //Whenever no one wins/has a superior choice.
                outcome = "It's a draw!";
            }
            roundCounter++;

            //Printing the round's result.
            System.out.println("Human chose " + userChoice + ", computer chose " + computerChoice + ". " + outcome);
            //Printing the players' scores.
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            
            //Figure out if user wants to continue playing.
            String userContinue = readInput("Do you wish to continue playing? (y/n)?");

            if (userContinue.equals("n")) break;
        }
        System.out.println("Bye bye :)");
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    public boolean isWinner(String choice1, String choice2) {
        if (choice1.equals("rock")) {
            return choice2.equals("scissors");
        }else if (choice1.equals("paper")){
            return choice2.equals("rock");
        }else {
            return choice2.equals("paper");
        }
    }

    public String getUserChoice() {
        String choice;

        //Ask for the user's choice.
        choice = readInput("Your choice (Rock/Paper/Scissors)?");

        return choice;
    }

    public String getComputerChoice() {
        //Highest limit of number.
        int randUpper = 2;
        //random object.
        Random rand = new Random();

        int randInt = rand.nextInt(randUpper);

        String computerChoice = rpsChoices.get(randInt);

        return computerChoice;
    }
}
